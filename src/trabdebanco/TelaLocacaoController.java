/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabdebanco;

import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import trabdebanco.model.ClassificacaoIndicativa;
import trabdebanco.model.Cliente;
import trabdebanco.model.Devedor;
import trabdebanco.model.Filme;
import trabdebanco.model.Locacao;

/**
 * FXML Controller class
 *
 * @author Dezordi
 */
public class TelaLocacaoController implements Initializable {

    @FXML
    private TableView<Locacao> tabLocacoes;
    @FXML
    private RadioButton rbDinheiro;
    @FXML
    private RadioButton rbMaster;
    @FXML
    private TextField txDataLoc;
    @FXML
    private TextField txDataEnt;
    @FXML
    private ComboBox<Cliente> cbClassificacaoCliente;
    private ObservableList<Locacao> locacoes;
    @FXML
    private TableColumn<Locacao, Integer> idCol;
    @FXML
    private TableColumn<Locacao, Filme> filmeCol;
    @FXML
    private TableColumn<Locacao, Cliente> clienteCol;
    @FXML
    private TableColumn<Locacao, Double> valorCol;
    @FXML
    private Button buscarPag;
    @FXML
    private ToggleGroup pag;
    @FXML
    private Button botValorPago;
    @FXML
    private TableView<Locacao> tabValorPago;
    @FXML
    private TableColumn<Locacao, Double> valorCol2;
    @FXML
    private TableColumn<Locacao, Cliente> clienteCol2;
    private ObservableList<Locacao> locacoes2;
    @FXML
    private TableView<ClassificacaoIndicativa> tabClassi;
    
    private ObservableList<ClassificacaoIndicativa> classis;
    @FXML
    private TableColumn<ClassificacaoIndicativa, String> classiNomeCol;
    @FXML
    private TableColumn<ClassificacaoIndicativa, Integer> classiIdadeCol;
    @FXML
    private TableView<Locacao> tabDataLoc;
    @FXML
    private TableColumn<Locacao, Date> dataEntCol;
    @FXML
    private TableColumn<Locacao, Integer> idLocCol;
    @FXML
    private Button botDataEnt;
    private ObservableList<Locacao> locDatas;
    private ObservableList<Devedor> devedores;
    @FXML
    private TableView<Devedor> tabDevedores;
    @FXML
    private TableColumn<Devedor, String> tabNomeCli;
    @FXML
    private TableColumn<Devedor, Date> tabDataCli;
    @FXML
    private TableColumn<Devedor, Double> tabValorCli;
    @FXML
    private TableColumn<Devedor, String> tabEmailCli;
    @FXML
    private Button botVoltar;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        tabValorPago.setVisible(false);
        tabLocacoes.setVisible(true);
        tabDataLoc.setVisible(false);
        tabClassi.setVisible(false);
        tabDevedores.setVisible(false);
        locacoes = tabLocacoes.getItems(); 
        locacoes.clear();        
        idCol.setCellValueFactory(new PropertyValueFactory<>("id"));      
        filmeCol.setCellValueFactory(new PropertyValueFactory<>("f")); 
        clienteCol.setCellValueFactory(new PropertyValueFactory<>("c"));
        valorCol.setCellValueFactory(new PropertyValueFactory<>("valor"));
        
        ArrayList<Locacao> locacoesBanco = Locacao.getAll();
        for(Locacao l: locacoesBanco){  
                locacoes.add(l);
        }
        ArrayList<Cliente> clientesBanco = Cliente.getAll();
        cbClassificacaoCliente.getItems().addAll(clientesBanco);
    }    

    @FXML
    private void botLporPagamento(ActionEvent event) {
        tabValorPago.setVisible(false);
        tabLocacoes.setVisible(true);
        tabDataLoc.setVisible(false);
        tabClassi.setVisible(false);
        tabDevedores.setVisible(false);
        locacoes = tabLocacoes.getItems(); 
        locacoes.clear();        
        idCol.setCellValueFactory(new PropertyValueFactory<>("id"));      
        filmeCol.setCellValueFactory(new PropertyValueFactory<>("f")); 
        clienteCol.setCellValueFactory(new PropertyValueFactory<>("c"));
        valorCol.setCellValueFactory(new PropertyValueFactory<>("valor"));
        
        if(rbDinheiro.isSelected()){
            ArrayList<Locacao> locacoesBanco = Locacao.locacaoPorPagamento("'dinheiro'");
        for(Locacao l: locacoesBanco){  
                locacoes.add(l);
        }
        }else if(rbMaster.isSelected()){
            ArrayList<Locacao> locacoesBanco = Locacao.locacaoPorPagamento("'mastercard'");
            for(Locacao l: locacoesBanco){  
                locacoes.add(l);
            }
        }
    }
    
    @FXML
    private void botCporDivida(ActionEvent event) {
        tabValorPago.setVisible(false);
        tabLocacoes.setVisible(false);
        tabClassi.setVisible(false);
        tabDataLoc.setVisible(false);
        tabDevedores.setVisible(true);
        devedores = tabDevedores.getItems(); 
        devedores .clear();        
        tabNomeCli.setCellValueFactory(new PropertyValueFactory<>("nome"));
        tabDataCli.setCellValueFactory(new PropertyValueFactory<>("dataEntrega"));
        tabValorCli.setCellValueFactory(new PropertyValueFactory<>("valor"));
        tabEmailCli.setCellValueFactory(new PropertyValueFactory<>("email"));
        
        ArrayList<Devedor> devedoresBanco = Devedor.getAll();
        for(Devedor d: devedoresBanco){  
                devedores.add(d);
        }
    }

    @FXML
    private void botLporDataLoc(ActionEvent event) throws ParseException {
        tabValorPago.setVisible(false);
        tabLocacoes.setVisible(true);
        tabDataLoc.setVisible(false);
        tabClassi.setVisible(false);
        tabDevedores.setVisible(false);
        locacoes = tabLocacoes.getItems(); 
        locacoes.clear();        
        idCol.setCellValueFactory(new PropertyValueFactory<>("id"));      
        filmeCol.setCellValueFactory(new PropertyValueFactory<>("f")); 
        clienteCol.setCellValueFactory(new PropertyValueFactory<>("c"));
        valorCol.setCellValueFactory(new PropertyValueFactory<>("valor"));
        
        ArrayList<Locacao> locacoesBanco = Locacao.locacaoPorDataLocacao(txDataLoc.getText());
        for(Locacao l: locacoesBanco){  
                locacoes.add(l);
        }
    }

    @FXML
    private void botLporDataEnt(ActionEvent event) {
        tabValorPago.setVisible(false);
        tabLocacoes.setVisible(true);
        tabDataLoc.setVisible(false);
        tabClassi.setVisible(false);
        tabDevedores.setVisible(false);
        locacoes = tabLocacoes.getItems(); 
        locacoes.clear();        
        idCol.setCellValueFactory(new PropertyValueFactory<>("id"));      
        filmeCol.setCellValueFactory(new PropertyValueFactory<>("f")); 
        clienteCol.setCellValueFactory(new PropertyValueFactory<>("c"));
        valorCol.setCellValueFactory(new PropertyValueFactory<>("valor"));
        
        ArrayList<Locacao> locacoesBanco = Locacao.locacaoPorDataEntrega(txDataEnt.getText());
        for(Locacao l: locacoesBanco){  
                locacoes.add(l);
        }
    }

    @FXML
    private void botVporClientes(ActionEvent event) {
        tabValorPago.setVisible(true);
        tabLocacoes.setVisible(false);
        tabDataLoc.setVisible(false);
        tabClassi.setVisible(false);
        tabDevedores.setVisible(false);
        locacoes2 = tabValorPago.getItems(); 
        locacoes2.clear();        
        clienteCol2.setCellValueFactory(new PropertyValueFactory<>("c"));
        valorCol2.setCellValueFactory(new PropertyValueFactory<>("valor"));
        
        ArrayList<Locacao> locacoesBanco2 = Locacao.valorPagoClientes();
        for(Locacao l: locacoesBanco2){  
                locacoes2.add(l);
        }
    }

    @FXML
    private void botCporCliente(ActionEvent event) {
        tabValorPago.setVisible(false);
        tabLocacoes.setVisible(false);
        tabDataLoc.setVisible(false);
        tabClassi.setVisible(true);
        tabDevedores.setVisible(false);
        classis = tabClassi.getItems(); 
        classis.clear();        
        classiNomeCol.setCellValueFactory(new PropertyValueFactory<>("nome"));
        classiIdadeCol.setCellValueFactory(new PropertyValueFactory<>("idade"));
        Cliente c = (Cliente) cbClassificacaoCliente.getSelectionModel().getSelectedItem();
        Date d = c.getDataNasc();
        int anoNasc = d.getYear() + 1900;
        java.util.Date dataAtual = new java.util.Date(); 
        java.sql.Date dataSQL = new java.sql.Date(dataAtual.getTime());
        int anoAtual = dataSQL.getYear() + 1900;
        
        ArrayList<ClassificacaoIndicativa> classisBanco = ClassificacaoIndicativa.porCliente(anoAtual - anoNasc);
        for(ClassificacaoIndicativa ci: classisBanco){  
               classis.add(ci);
        }
    }

    @FXML
    private void botDporLocacao(ActionEvent event) {
        tabValorPago.setVisible(false);
        tabLocacoes.setVisible(false);
        tabClassi.setVisible(false);
        tabDataLoc.setVisible(true);
        tabDevedores.setVisible(false);
        locDatas = tabDataLoc.getItems(); 
        locDatas .clear();        
        dataEntCol.setCellValueFactory(new PropertyValueFactory<>("dataEntrega"));
        idLocCol.setCellValueFactory(new PropertyValueFactory<>("id"));
        
        ArrayList<Locacao> locaDataEnt = Locacao.dataEntPorLoc();
        for(Locacao l: locaDataEnt){  
                locDatas.add(l);
        }
    }

    @FXML
    private void clicouEmVoltar(ActionEvent event) {
        Parent root;
        try {
            Stage stage = TrabDeBanco.stage;
            
            FXMLLoader loader = new FXMLLoader(getClass().getResource("Menu.fxml"));
            root = loader.load();
            MenuController telaMenu = loader.getController();
            
            Scene scene = new Scene(root);
            
            stage.setScene(scene);          
            
            } catch (NullPointerException | IOException ex) {
                System.out.println("Erro Arquivo FXML");
            }
    }

}
