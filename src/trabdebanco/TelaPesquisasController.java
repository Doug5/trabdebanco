/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabdebanco;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import trabdebanco.model.Filme;
import trabdebanco.model.ClassificacaoIndicativa;
import trabdebanco.model.Genero;

/**
 * FXML Controller class
 *
 * @author Dezordi
 */
public class TelaPesquisasController implements Initializable {

    @FXML
    private TableView<Filme> tabFilme;
    @FXML
    private TableColumn<Filme, String> nomeCol;
    @FXML
    private TableColumn<Filme, String> statusCol;
    @FXML
    private TableColumn<Filme, ClassificacaoIndicativa> clasiCol;
    private ObservableList<Filme> filmes;
    @FXML
    private ComboBox<Genero> cbGeneros;
    @FXML
    private Button buscarGenero;
    @FXML
    private ComboBox<ClassificacaoIndicativa> cbClassificacoes;
    @FXML
    private Button buscarClassificacoes;
    @FXML
    private RadioButton rbDisp;
    @FXML
    private RadioButton rbAlug;
    @FXML
    private Button buscarStatus;
    @FXML
    private ToggleGroup status;
    @FXML
    private Button buscarLancamento;
    @FXML
    private TextField tfLancamento;
    @FXML
    private Button botFllmesSemGenero;
    @FXML
    private Button botVoltar;
    @FXML
    private ComboBox<Filme> cbFilmes;
    @FXML
    private Button buscarGenerosFilme;
    @FXML
    private TableView<Genero> generosFilmeTable;
    @FXML
    private TableColumn<Genero, String> colunaDeGenerosPF;
    private ObservableList<Genero> generosPorF;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        filmes = tabFilme.getItems(); 
        filmes.clear();        
        nomeCol.setCellValueFactory(new PropertyValueFactory<>("nome"));      
        statusCol.setCellValueFactory(new PropertyValueFactory<>("status")); 
        clasiCol.setCellValueFactory(new PropertyValueFactory<>("ci"));
        
        ArrayList<Filme> filmesBanco = Filme.getAll();
        for(Filme f: filmesBanco){  
                filmes.add(f);
        }
        
        ArrayList<Filme> filmesBancoCB = Filme.getAll();
        cbFilmes.getItems().addAll(filmesBancoCB);
        
        ArrayList<Genero> generosBanco = Genero.getAll();
        cbGeneros.getItems().addAll(generosBanco);
        
        ArrayList<ClassificacaoIndicativa> classificacoesBanco = ClassificacaoIndicativa.getAll();
        cbClassificacoes.getItems().addAll(classificacoesBanco);

    }    

    @FXML
    private void botFporOrdem(ActionEvent event) {
        tabFilme.setVisible(true);
        generosFilmeTable.setVisible(false);
        filmes = tabFilme.getItems(); 
        filmes.clear();        
        nomeCol.setCellValueFactory(new PropertyValueFactory<>("nome"));      
        statusCol.setCellValueFactory(new PropertyValueFactory<>("status")); 
        clasiCol.setCellValueFactory(new PropertyValueFactory<>("ci"));
        
        ArrayList<Filme> filmesBanco = Filme.selectOrdemAlfabetica();
        for(Filme f: filmesBanco){  
                filmes.add(f);
        }
    }


    @FXML
    private void botFporGenero(ActionEvent event) {
        tabFilme.setVisible(true);
        generosFilmeTable.setVisible(false);
        filmes = tabFilme.getItems(); 
        filmes.clear();        
        nomeCol.setCellValueFactory(new PropertyValueFactory<>("nome"));      
        statusCol.setCellValueFactory(new PropertyValueFactory<>("status")); 
        clasiCol.setCellValueFactory(new PropertyValueFactory<>("ci"));
        
        ArrayList<Filme> filmesBanco = Filme.selectPorGenero((Genero)cbGeneros.getSelectionModel().getSelectedItem());
        for(Filme f: filmesBanco){  
                filmes.add(f);
        }
    }
    
    @FXML
    private void botFporClassificacao(ActionEvent event) {
        tabFilme.setVisible(true);
        generosFilmeTable.setVisible(false);
        filmes = tabFilme.getItems(); 
        filmes.clear();        
        nomeCol.setCellValueFactory(new PropertyValueFactory<>("nome"));      
        statusCol.setCellValueFactory(new PropertyValueFactory<>("status")); 
        clasiCol.setCellValueFactory(new PropertyValueFactory<>("ci"));
        
        ArrayList<Filme> filmesBanco = Filme.selectPorClassificacao((ClassificacaoIndicativa)cbClassificacoes.getSelectionModel().getSelectedItem());
        for(Filme f: filmesBanco){  
                filmes.add(f);
        }
    }

    @FXML
    private void botFporStatus(ActionEvent event) {
        tabFilme.setVisible(true);
        generosFilmeTable.setVisible(false);
        filmes = tabFilme.getItems(); 
        filmes.clear();        
        nomeCol.setCellValueFactory(new PropertyValueFactory<>("nome"));      
        statusCol.setCellValueFactory(new PropertyValueFactory<>("status")); 
        clasiCol.setCellValueFactory(new PropertyValueFactory<>("ci"));
        if(rbDisp.isSelected()){
            ArrayList<Filme> filmesBanco = Filme.selectPorStatus(1);
            for(Filme f: filmesBanco){  
                filmes.add(f);
            }
        }else if(rbAlug.isSelected()){
           ArrayList<Filme> filmesBanco = Filme.selectPorStatus(0);
            for(Filme f: filmesBanco){  
                filmes.add(f);
            } 
        }
    }

    @FXML
    private void botFporLancamento(ActionEvent event) {
        tabFilme.setVisible(true);
        generosFilmeTable.setVisible(false);
        filmes = tabFilme.getItems(); 
        filmes.clear();        
        nomeCol.setCellValueFactory(new PropertyValueFactory<>("nome"));      
        statusCol.setCellValueFactory(new PropertyValueFactory<>("status")); 
        clasiCol.setCellValueFactory(new PropertyValueFactory<>("ci"));
        
        ArrayList<Filme> filmesBanco = Filme.selectPorLancamento(tfLancamento.getText());
        for(Filme f: filmesBanco){  
                filmes.add(f);
        }
    }

    @FXML
    private void clicouFllmesSemGenero(ActionEvent event) {
        tabFilme.setVisible(true);
        generosFilmeTable.setVisible(false);
        filmes = tabFilme.getItems(); 
        filmes.clear();        
        nomeCol.setCellValueFactory(new PropertyValueFactory<>("nome"));      
        statusCol.setCellValueFactory(new PropertyValueFactory<>("status")); 
        clasiCol.setCellValueFactory(new PropertyValueFactory<>("ci"));
        
        ArrayList<Filme> filmesBanco = Filme.filmesSemGenero();
        for(Filme f: filmesBanco){  
                filmes.add(f);
        }
    }

    @FXML
    private void clicouVoltar(ActionEvent event) {
        tabFilme.setVisible(true);
        generosFilmeTable.setVisible(false);
        Parent root;
        try {
            Stage stage = TrabDeBanco.stage;
            
            FXMLLoader loader = new FXMLLoader(getClass().getResource("Menu.fxml"));
            root = loader.load();
            MenuController telaMenu = loader.getController();
            
            Scene scene = new Scene(root);
            
            stage.setScene(scene);          
            
            } catch (NullPointerException | IOException ex) {
                System.out.println("Erro Arquivo FXML");
            }
    }

    @FXML
    private void botGporF(ActionEvent event) {
        generosFilmeTable.setVisible(true);
        tabFilme.setVisible(false);
        generosPorF = generosFilmeTable.getItems(); 
        generosPorF.clear();        
        colunaDeGenerosPF.setCellValueFactory(new PropertyValueFactory<>("nome"));      
        Filme filmin = (Filme) cbFilmes.getSelectionModel().getSelectedItem();
        colunaDeGenerosPF.setText(filmin.getNome());
        ArrayList<Genero> generosPorFBanco = Genero.getGenerosPorF(filmin.getId());
        for(Genero g: generosPorFBanco){  
                generosPorF.add(g);
        }
    }



    
}
