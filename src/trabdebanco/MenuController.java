/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabdebanco;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Dezordi
 */
public class MenuController implements Initializable {

    @FXML
    private Button botLocacoes;
    @FXML
    private Button botFilmes;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void irTelaLocacoes(ActionEvent event) {
        Parent root;
        try {
            Stage stage = TrabDeBanco.stage;
            
            FXMLLoader loader = new FXMLLoader(getClass().getResource("TelaLocacao.fxml"));
            root = loader.load();
            TelaLocacaoController telaLoc = loader.getController();
            
            Scene scene = new Scene(root);
            
            stage.setScene(scene);          
            
            } catch (NullPointerException | IOException ex) {
                System.out.println("Erro Arquivo FXML");
            }
    }

    @FXML
    private void irTelaFilmes(ActionEvent event) {
        Parent root;
        try {
            Stage stage = TrabDeBanco.stage;
            
            FXMLLoader loader = new FXMLLoader(getClass().getResource("TelaPesquisas.fxml"));
            root = loader.load();
            TelaPesquisasController telaPesq = loader.getController();
            
            Scene scene = new Scene(root);
            
            stage.setScene(scene);          
            
            } catch (NullPointerException | IOException ex) {
                System.out.println("Erro Arquivo FXML");
            }
    }
    }
   
