/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabdebanco.model;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author usuario
 */
public class Locacao {
    private int id;
    private Date dataEntrega;
    private Date dataLocacao;
    private Filme f;
    private Cliente c;
    private double valor;
    private String pagamento;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDataEntrega() {
        return dataEntrega;
    }

    public void setDataEntrega(Date dataEntrega) {
        this.dataEntrega = dataEntrega;
    }

    public Date getDataLocacao() {
        return dataLocacao;
    }

    public void setDataLocacao(Date dataLocacao) {
        this.dataLocacao = dataLocacao;
    }

    public Filme getF() {
        return f;
    }

    public void setF(Filme f) {
        this.f = f;
    }

    public Cliente getC() {
        return c;
    }

    public void setC(Cliente c) {
        this.c = c;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public String getPagamento() {
        return pagamento;
    }

    public void setPagamento(String pagamento) {
        this.pagamento = pagamento;
    }
    
    public static ArrayList<Locacao> getAll(){
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        ArrayList<Locacao> lista = new ArrayList<>();
        String selectSQL = "SELECT * FROM locadoro_locacao";
        Statement st;
        try{
            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(selectSQL);
            while(rs.next()){
                Locacao l = new Locacao();
                l.setId(rs.getInt("id_locacao"));
                l.setDataLocacao(rs.getDate("data_locacao"));
                l.setDataEntrega(rs.getDate("data_entrega"));
                l.setValor(rs.getDouble("valor"));
                l.setPagamento(rs.getString("pagamento"));
                Filme f = new Filme();
                f.getOne(rs.getInt("id_filme"));
                l.setF(f);
                Cliente cl = new Cliente();
                cl.getOne(rs.getInt("id_cliente"));
                l.setC(cl);
                lista.add(l);
            }
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            c.desconecta();
        }
        return lista;
    }
    
    public void getOne(int i){
        this.id = i;
        getOne();
    }
    public void getOne(){
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        String selectSQL = "SELECT * FROM locadoro_locacao WHERE id_locacao = ?";
        
        PreparedStatement ps;
        try{
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setInt(1, this.id);
            
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                this.setId(rs.getInt("id_locacao"));
            }
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            c.desconecta();
        }
    }
    
    public static ArrayList<Locacao> locacaoPorPagamento(String fPag){
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        ArrayList<Locacao> lista = new ArrayList<>();
        String selectSQL = "SELECT * FROM locadoro_locacao \n" +
"where \"pagamento\" = " + fPag;
        Statement st;
        try{
            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(selectSQL);
            while(rs.next()){
                Locacao l = new Locacao();
                l.setId(rs.getInt("id_locacao"));
                l.setDataLocacao(rs.getDate("data_locacao"));
                l.setDataEntrega(rs.getDate("data_entrega"));
                l.setValor(rs.getDouble("valor"));
                l.setPagamento(rs.getString("pagamento"));
                Filme f = new Filme();
                f.getOne(rs.getInt("id_filme"));
                l.setF(f);
                Cliente cl = new Cliente();
                cl.getOne(rs.getInt("id_cliente"));
                l.setC(cl);
                lista.add(l);
            }
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            c.desconecta();
        }
        return lista;
    }
    
    public static ArrayList<Locacao> locacaoPorDataLocacao(String data){
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        ArrayList<Locacao> lista = new ArrayList<>();
        String selectSQL = "SELECT * FROM locadoro_locacao \n" +
"where to_char(\"data_locacao\", 'DD/MM/YY') = " + "'" + data + "'";
        Statement st;
        try{
            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(selectSQL);
            while(rs.next()){
                Locacao l = new Locacao();
                l.setId(rs.getInt("id_locacao"));
                l.setDataLocacao(rs.getDate("data_locacao"));
                l.setDataEntrega(rs.getDate("data_entrega"));
                l.setValor(rs.getDouble("valor"));
                l.setPagamento(rs.getString("pagamento"));
                Filme f = new Filme();
                f.getOne(rs.getInt("id_filme"));
                l.setF(f);
                Cliente cl = new Cliente();
                cl.getOne(rs.getInt("id_cliente"));
                l.setC(cl);
                lista.add(l);
            }
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            c.desconecta();
        }
        return lista;
    }
    
    public static ArrayList<Locacao> locacaoPorDataEntrega(String data){
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        ArrayList<Locacao> lista = new ArrayList<>();
        String selectSQL = "SELECT * FROM locadoro_locacao \n" +
"where to_char(\"data_entrega\", 'DD/MM/YY') = " + "'" + data + "'";
        Statement st;
        try{
            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(selectSQL);
            while(rs.next()){
                Locacao l = new Locacao();
                l.setId(rs.getInt("id_locacao"));
                l.setDataLocacao(rs.getDate("data_locacao"));
                l.setDataEntrega(rs.getDate("data_entrega"));
                l.setValor(rs.getDouble("valor"));
                l.setPagamento(rs.getString("pagamento"));
                Filme f = new Filme();
                f.getOne(rs.getInt("id_filme"));
                l.setF(f);
                Cliente cl = new Cliente();
                cl.getOne(rs.getInt("id_cliente"));
                l.setC(cl);
                lista.add(l);
            }
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            c.desconecta();
        }
        return lista;
    }
    
    public static ArrayList<Locacao> valorPagoClientes(){
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        ArrayList<Locacao> lista = new ArrayList<>();
        String selectSQL = "select locadoro_cliente.\"Id_cliente\", locadoro_cliente.\"Nome_cliente\", locadoro_locacao.\"id_locacao\" ,locadoro_locacao.\"valor\"\n" +
"from locadoro_cliente\n" +
"inner join locadoro_locacao on locadoro_cliente.\"Id_cliente\" = locadoro_locacao.\"id_cliente\"\n" +
"order by locadoro_cliente.\"Nome_cliente\"";
        Statement st;
        try{
            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(selectSQL);
            while(rs.next()){
                Locacao l = new Locacao();
                l.setId(rs.getInt("id_locacao"));
                l.setValor(rs.getDouble("valor"));
                Cliente cl = new Cliente();
                cl.getOne(rs.getInt("id_cliente"));
                l.setC(cl);
                lista.add(l);
            }
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            c.desconecta();
        }
        return lista;
    }
    
    public static ArrayList<Locacao> dataEntPorLoc(){
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        ArrayList<Locacao> lista = new ArrayList<>();
        String selectSQL = "select locadoro_locacao.\"data_entrega\", locadoro_locacao.\"id_locacao\"\n" +
"from locadoro_locacao\n" +
"order by locadoro_locacao.\"id_locacao\"";
        Statement st;
        try{
            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(selectSQL);
            while(rs.next()){
                Locacao l = new Locacao();
                l.setId(rs.getInt("id_locacao"));
                l.setDataEntrega(rs.getDate("data_entrega"));
                lista.add(l);
            }
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            c.desconecta();
        }
        return lista;
    }
}
