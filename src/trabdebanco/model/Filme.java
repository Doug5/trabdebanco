/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabdebanco.model;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author usuario
 */
public class Filme {
    private int id;
    private String nome;
    private Date lancamento;
    private boolean status;
    private ClassificacaoIndicativa ci;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Date getLancamento() {
        return lancamento;
    }

    public void setLancamento(Date lancamento) {
        this.lancamento = lancamento;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public ClassificacaoIndicativa getCi() {
        return ci;
    }

    public void setCi(ClassificacaoIndicativa ci) {
        this.ci = ci;
    }
    
    public static ArrayList<Filme> getAll(){
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        ArrayList<Filme> lista = new ArrayList<>();
        String selectSQL = "SELECT * FROM LOCADORO_FILME";
        Statement st;
        try{
            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(selectSQL);
            while(rs.next()){
                Filme f = new Filme();
                f.setId(rs.getInt("Id_filme"));
                f.setNome(rs.getString("Nome_filme"));
                ClassificacaoIndicativa ci = new ClassificacaoIndicativa();
                ci.setId(rs.getInt("Id_classificacao"));
                ci.getOne();
                f.setCi(ci);
                f.setLancamento(rs.getDate("lancamento"));
                f.setStatus(rs.getBoolean("status"));
                lista.add(f);
            }
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            c.desconecta();
        }
        return lista;
    }
    
    public void getOne(int i){
        this.id = i;
        getOne();
    }
    public void getOne(){
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        String selectSQL = "SELECT * FROM locadoro_filme WHERE \"Id_filme\" = ?";
        
        PreparedStatement ps;
        try{
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setInt(1, this.id);
            
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                this.setId(rs.getInt("Id_filme"));
            }
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            c.desconecta();
        }
}
    public static ArrayList<Filme> selectOrdemAlfabetica(){
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        ArrayList<Filme> lista = new ArrayList<>();
        String selectSQL = "SELECT * FROM locadoro_filme\n" +
"order by locadoro_filme.\"Nome_filme\"";
        
        Statement st;
        try{
            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(selectSQL);
            while(rs.next()){
                Filme f = new Filme();
                f.setId(rs.getInt("id_filme"));
                f.setNome(rs.getString("Nome_filme"));
                ClassificacaoIndicativa ci = new ClassificacaoIndicativa();
                ci.setId(rs.getInt("id_classificacao"));
                ci.getOne();
                f.setCi(ci);
                f.setLancamento(rs.getDate("lancamento"));
                f.setStatus(rs.getBoolean("status"));
                lista.add(f);
            }
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            c.desconecta();
        }
        return lista;
    }
    public static ArrayList<Filme> selectPorGenero(Genero g){
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        ArrayList<Filme> lista = new ArrayList<>();
        int id = g.getId();
        String selectSQL = "SELECT * from locadoro_filme\n" +
"inner join locadoro_f_genero on locadoro_filme.\"Id_filme\" = locadoro_f_genero.id_filme\n" +
"where locadoro_f_genero.id_genero = " + id;
              
        Statement st;
        try{
            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(selectSQL);
            while(rs.next()){
                Filme f = new Filme();
                f.setId(rs.getInt("id_filme"));
                f.setNome(rs.getString("Nome_filme"));
                ClassificacaoIndicativa ci = new ClassificacaoIndicativa();
                ci.setId(rs.getInt("id_classificacao"));
                ci.getOne();
                f.setCi(ci);
                f.setLancamento(rs.getDate("lancamento"));
                f.setStatus(rs.getBoolean("status"));
                lista.add(f);
            }
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            c.desconecta();
        }
        return lista;
    }
    
    public static ArrayList<Filme> selectPorClassificacao(ClassificacaoIndicativa clai){
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        ArrayList<Filme> lista = new ArrayList<>();
        int id = clai.getId();
        String selectSQL = "SELECT * from locadoro_filme\n" +
"where \"Id_classificacao\" = " + id;
              
        Statement st;
        try{
            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(selectSQL);
            while(rs.next()){
                Filme f = new Filme();
                f.setId(rs.getInt("id_filme"));
                f.setNome(rs.getString("Nome_filme"));
                ClassificacaoIndicativa ci = new ClassificacaoIndicativa();
                ci.setId(rs.getInt("id_classificacao"));
                ci.getOne();
                f.setCi(ci);
                f.setLancamento(rs.getDate("lancamento"));
                f.setStatus(rs.getBoolean("status"));
                lista.add(f);
            }
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            c.desconecta();
        }
        return lista;
    }
    
    public static ArrayList<Filme> selectPorStatus(int status){
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        ArrayList<Filme> lista = new ArrayList<>();
        String selectSQL = "SELECT * from locadoro_filme\n" +
"where \"status\" = " + status;
              
        Statement st;
        try{
            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(selectSQL);
            while(rs.next()){
                Filme f = new Filme();
                f.setId(rs.getInt("id_filme"));
                f.setNome(rs.getString("Nome_filme"));
                ClassificacaoIndicativa ci = new ClassificacaoIndicativa();
                ci.setId(rs.getInt("id_classificacao"));
                ci.getOne();
                f.setCi(ci);
                f.setLancamento(rs.getDate("lancamento"));
                f.setStatus(rs.getBoolean("status"));
                lista.add(f);
            }
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            c.desconecta();
        }
        return lista;
    }
    
    public static ArrayList<Filme> selectPorLancamento(String ano){
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        ArrayList<Filme> lista = new ArrayList<>();
        String selectSQL = "select * from locadoro_filme\n" +
"where to_char(\"lancamento\",'yyyy') = " + ano;
              
        Statement st;
        try{
            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(selectSQL);
            while(rs.next()){
                Filme f = new Filme();
                f.setId(rs.getInt("id_filme"));
                f.setNome(rs.getString("Nome_filme"));
                ClassificacaoIndicativa ci = new ClassificacaoIndicativa();
                ci.setId(rs.getInt("id_classificacao"));
                ci.getOne();
                f.setCi(ci);
                f.setLancamento(rs.getDate("lancamento"));
                f.setStatus(rs.getBoolean("status"));
                lista.add(f);
            }
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            c.desconecta();
        }
        return lista;
    }
    
    public static ArrayList<Filme> filmesSemGenero(){
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        ArrayList<Filme> lista = new ArrayList<>();
        String selectSQL = "SELECT * from locadoro_filme\n" +
"where locadoro_filme.\"Id_filme\" not in(SELECT locadoro_f_genero.id_filme from locadoro_f_genero)";
              
        Statement st;
        try{
            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(selectSQL);
            while(rs.next()){
                Filme f = new Filme();
                f.setId(rs.getInt("id_filme"));
                ClassificacaoIndicativa ci = new ClassificacaoIndicativa();
                ci.setId(rs.getInt("id_classificacao"));
                ci.getOne();
                f.setCi(ci);
                f.setLancamento(rs.getDate("lancamento"));
                f.setStatus(rs.getBoolean("status"));
                f.setNome(rs.getString("Nome_filme"));
                lista.add(f);
            }
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            c.desconecta();
        }
        return lista;
    }
    
    @Override
    public String toString() {
        return "id_filme: " + id + " Nome: " + nome;
    }
}
