/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabdebanco.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author usuario
 */
public class Genero {
    private int id;
    private String desc;
    private String nome;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    
    public static ArrayList<Genero> getAll(){
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        ArrayList<Genero> lista = new ArrayList<>();
        String selectSQL = "SELECT * FROM locadoro_genero";
        Statement st;
        try{
            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(selectSQL);
            while(rs.next()){
                Genero g = new Genero();
                g.setId(rs.getInt("id_genero"));
                g.setDesc(rs.getString("descricao_genero"));
                g.setNome(rs.getString("Nome_genero"));
                lista.add(g);
            }
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            c.desconecta();
        }
        return lista;
    }
    
    public void getOne(int i){
        this.id = i;
        getOne();
    }
    public void getOne(){
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        String selectSQL = "SELECT * FROM locadoro_genero WHERE id_genero = ?";
        
        PreparedStatement ps;
        try{
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setInt(1, this.id);
            
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                this.setId(rs.getInt("id_genero"));
            }
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            c.desconecta();
        }
}
    public static ArrayList<Genero> getGenerosPorF(int id_filme){
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        ArrayList<Genero> lista = new ArrayList<>();
        String selectSQL = "select * from locadoro_genero\n" +
"inner join LOCADORO_F_GENERO on locadoro_genero.\"id_genero\" = LOCADORO_F_GENERO.id_genero\n" +
"where LOCADORO_F_GENERO.id_filme = " + id_filme;
        Statement st;
        try{
            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(selectSQL);
            while(rs.next()){
                Genero g = new Genero();
                g.setId(rs.getInt("id_genero"));
                g.setDesc(rs.getString("descricao_genero"));
                g.setNome(rs.getString("Nome_genero"));
                lista.add(g);
            }
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            c.desconecta();
        }
        return lista;
    }

    @Override
    public String toString() {
        return nome;
    }
    
}
