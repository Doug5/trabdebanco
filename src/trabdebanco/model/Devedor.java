/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabdebanco.model;

import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author usuario
 */
public class Devedor {
    private String nome;
    private Date dataEntrega;
    private double valor;
    private String email;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Date getDataEntrega() {
        return dataEntrega;
    }

    public void setDataEntrega(Date dataEntrega) {
        this.dataEntrega = dataEntrega;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    public static ArrayList<Devedor> getAll(){
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        ArrayList<Devedor> lista = new ArrayList<>();
        String selectSQL = "select locadoro_cliente.\"Nome_cliente\", locadoro_locacao.\"data_entrega\" as \"Data de Entrega(Pendente)\", locadoro_locacao.\"valor\", locadoro_cliente.\"email\"\n" +
"from locadoro_locacao\n" +
"inner join locadoro_cliente on locadoro_cliente.\"Id_cliente\" = locadoro_locacao.\"id_cliente\"\n" +
"where \"data_entrega\" > (select to_date(sysdate, 'DD/MM/YY') from dual)";
        Statement st;
        try{
            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(selectSQL);
            while(rs.next()){
                Devedor d = new Devedor();
                d.setNome(rs.getString("Nome_cliente"));
                d.setDataEntrega(rs.getDate("Data de Entrega(Pendente)"));
                d.setValor(rs.getDouble("valor"));
                d.setEmail(rs.getString("email"));
                lista.add(d);
            }
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            c.desconecta();
        }
        return lista;
    }

    @Override
    public String toString() {
        return "Devedor{" + "nome=" + nome + '}';
    }
    
    
}
