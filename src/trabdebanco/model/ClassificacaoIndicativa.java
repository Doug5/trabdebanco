

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabdebanco.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author usuario
 */
public class ClassificacaoIndicativa {
    private int id;
    private String nome;
    private int idade;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }
    
    public static ArrayList<ClassificacaoIndicativa> getAll(){
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        ArrayList<ClassificacaoIndicativa> lista = new ArrayList<>();
        String selectSQL = "SELECT * FROM locadoro_classificacao";
        Statement st;
        try{
            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(selectSQL);
            while(rs.next()){
                ClassificacaoIndicativa ci = new ClassificacaoIndicativa();
                ci.setId(rs.getInt("id_classificacao"));
                ci.setIdade(rs.getInt("idade_classificacao"));
                ci.setNome(rs.getString("Nome_classificacao"));
                lista.add(ci);
            }
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            c.desconecta();
        }
        return lista;
    }
    
    public void getOne(int i){
        this.id = i;
        getOne();
    }
    public void getOne(){
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        String selectSQL = "SELECT * FROM locadoro_classificacao WHERE \"Id_classificacao\" = ?";
        
        PreparedStatement ps;
        try{
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setInt(1, this.id);
            
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                this.setId(rs.getInt("Id_classificacao"));
            }
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            c.desconecta();
        }
    }
    
    public static ArrayList<ClassificacaoIndicativa> porCliente(int idade){
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        ArrayList<ClassificacaoIndicativa> lista = new ArrayList<>();
        String selectSQL = "SELECT LOCADORO_CLASSIFICACAO.\"Nome_classificacao\", LOCADORO_CLASSIFICACAO.\"idade_classificacao\"\n" +
"FROM locadoro_classificacao\n" +
"where LOCADORO_CLASSIFICACAO.\"idade_classificacao\" <= " + idade;
        Statement st;
        try{
            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(selectSQL);
            while(rs.next()){
                ClassificacaoIndicativa ci = new ClassificacaoIndicativa();
                ci.setIdade(rs.getInt("idade_classificacao"));
                ci.setNome(rs.getString("Nome_classificacao"));
                lista.add(ci);
            }
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            c.desconecta();
        }
        return lista;
    }

    @Override
    public String toString() {
        return "ClassificacaoIndicativa{" + "id=" + id + ", nome=" + nome + ", idade=" + idade + '}';
    }

        
}
